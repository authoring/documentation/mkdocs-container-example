# Título doutro documento do subcapítulo 1 A

Este documento contén unha imaxe en galego.

## Localización automática de arquivos multimedia / link / asset

![Bandeira galega](static/bandeira_galega.jpeg)

A imaxe fonte é localizada dinámicamente cando está sendo referenciada polo markdown fonte da páxina como `![calqueira cousa](imaxe.png)`. A imaxe ten que estar no mesmo directorio que o documento.
